releases:
  - name: alerting
    namespace: alerting
    chart: ./chart
    values:
      - domain: alerting.{{ .Values.cluster.domain }}
        cluster_domain: "{{ .Values.cluster.domain }}"
        basic_auth: "{{ .Values.cluster.basic_auth }}"
        tls_wildcard_secret: "{{ .Values.cluster.tls_wildcard_secret }}"
        disk_type: "{{ .Values.hardware.diskType }}"
        disk1: "{{ .Values.hardware.disk1 }}"
        disk2: "{{ .Values.hardware.disk2 }}"
        nodes: {{ toJson .Values.nodes }}
        kubevirt_installed: {{ .Values.services.alerting.check_kubevirt }}
