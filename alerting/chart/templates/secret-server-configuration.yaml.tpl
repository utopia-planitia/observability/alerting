apiVersion: v1
kind: Secret
metadata:
  name: server-configuration
type: Opaque
stringData:
{{ (tpl (.Files.Glob "secrets/server-configuration/*").AsConfig . ) | indent 2 }}
