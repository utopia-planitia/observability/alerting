#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
set -o xtrace

# https://letsencrypt.org/docs/staging-environment/
for CERTIFICATE in \
  letsencrypt-stg-root-x2.pem \
  letsencrypt-stg-root-x1.pem; do
  curl \
    --fail \
    --location \
    --output "/usr/local/share/ca-certificates/${CERTIFICATE:?}" \
    --show-error \
    --silent \
    "https://letsencrypt.org/certs/staging/${CERTIFICATE:?}"
done

update-ca-certificates

curl \
  --connect-timeout 30 \
  --fail \
  --head \
  --max-time 30 \
  --output /dev/stderr \
  --show-error \
  --silent \
  --write-out '%{http_code}' \
  'http://{{ .Values.domain }}' |
  grep -Fqx 308
