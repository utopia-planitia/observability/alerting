#!/usr/bin/env bats

teardown () {
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

setup () {
  set -eo pipefail
}

@test "all icinga tests pass" {
  LOOPS=0
  while ! verifyTests;
  do
    sleep 10;
    if [ $LOOPS -eq 60 ]; then

      # print a list of services that are not OK
      run curl --max-time 30 --connect-timeout 30 --fail --location --show-error --silent 'http://server/cgi-bin/icinga/status.cgi?host=all&servicestatustypes=28&csvoutput'

      [ "icinga checks do not validate" = "" ]
    fi
    LOOPS=$((LOOPS+1))
  done

  run verifyTests
  [ $status = "0" ]
}

verifyTests () {
  declare -a expectedMatches=(
    "title='Unacknowledged Hosts DOWN'> 0 <"
    "title='Acknowledged Hosts DOWN'> 0 <"
    "title='Handled Hosts DOWN'> 0 <"
    "title='Unacknowledged Hosts UNREACHABLE'> 0 <"
    "title='Acknowledged Hosts UNREACHABLE'> 0 <"
    "title='Handled Hosts UNREACHABLE'> 0 <"
    "title='All Hosts PENDING'> 0 PENDING <"
    "title='All Problem Hosts'> 0 /<"
    "title='Unacknowledged Services WARNING'> 0 <"
    "title='Acknowledged Services WARNING'> 0 <"
    "title='Handled Services WARNING'> 0 <"
    "title='Unacknowledged Services CRITICAL'> 0 <"
    "title='Acknowledged Services CRITICAL'> 0 <"
    "title='Handled Services CRITICAL'> 0 <"
    "title='Unacknowledged Services UNKNOWN'> 0 <"
    "title='Acknowledged Services UNKNOWN'> 0 <"
    "title='Handled Services UNKNOWN'> 0 <"
    "title='All Services PENDING'> 0 PENDING <"
    "title='All Problem Services'> 0 /<"
  );

  OUTPUT=$(curl --max-time 30 --connect-timeout 30 --fail --silent http://server/cgi-bin/icinga/tac.cgi\?tac_header)

  for expectedMatch in "${expectedMatches[@]}"; do
    if [[ ! "$OUTPUT" =~ "${expectedMatch}" ]]; then
      return 1
    fi
  done

  return 0
}
