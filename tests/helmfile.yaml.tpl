releases:
  - name: alerting-tests
    namespace: alerting
    chart: ./chart
    labels:
      phase: only-testing
    values:
      - domain: alerting.{{ .Values.cluster.domain }}
